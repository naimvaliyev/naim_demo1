# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
if ARGV[0] != 'plugin'
  
  required_plugins = ['vagrant-hostmanager', 'vagrant-reload',
                      'vagrant-cachier', 'vagrant-winnfsd', 'vagrant-faster'
  ]
  plugins_to_install = required_plugins.select { |plugin| not Vagrant.has_plugin? plugin }
  if not plugins_to_install.empty?
    puts "Installing plugins: #{plugins_to_install.join(' ')}"
      if system "vagrant plugin install #{plugins_to_install.join(' ')}"
        exec "vagrant #{ARGV.join(' ')}"
      else
        abort "Installation of one or more plugins has failed. Aborting."
      end

  end
end

if Vagrant::Util::Platform.windows?
  unless Vagrant.has_plugin?("vagrant-winnfsd")
    puts 'Installing vagrant-winnfsd Plugin...'
    system "vagrant plugin install vagrant-winnfsd"
  end
end

$global = <<-SCRIPT
#check for private key for vm-vm comm
[ -f /vagrant/id_rsa ] || {
  ssh-keygen -t rsa -f /vagrant/id_rsa -q -N ''
}
#deploy key
[ -f /home/vagrant/.ssh/id_rsa ] || {
    cp /vagrant/id_rsa /home/vagrant/.ssh/id_rsa
    chmod 0600 /home/vagrant/.ssh/id_rsa
}
#allow ssh passwordless
grep 'vagrant@node' ~/.ssh/authorized_keys &>/dev/null || {
  cat /vagrant/id_rsa.pub >> ~/.ssh/authorized_keys
  chmod 0600 ~/.ssh/authorized_keys
}
#exclude node* from host checking
cat > ~/.ssh/config <<EOF
Host node*
   StrictHostKeyChecking no
   UserKnownHostsFile=/dev/null
EOF

#end script
SCRIPT

$initScript = <<-SCRIPT
  export SCRIPT_HOME=/vagrant
  echo _______________________________________________________________________________
  echo 0. Prepare env mysql
  source $SCRIPT_HOME/env.sh
  echo _______________________________________________________________________________
  echo 1. Install and setup java
  $SCRIPT_HOME/java.sh
SCRIPT

$systemd = <<-SCRIPT

cat > /etc/systemd/system/petclinic.service <<EOF
[Unit]
Description=Petclinic Java Spring Boot
[Service]
User=vagrant
Environment="MYSQL_PASS=1Q2W3E4r5tzxc@"
Environment="MYSQL_URL=jdbc:mysql://db:3306/demo1"
Environment="MYSQL_USER=demo1"
[Service]
User=vagrant

WorkingDirectory=/home/vagrant/demo1


ExecStart=/bin/java -Xms128m -Xmx256m -Dspring.profiles.active=mysql -jar /home/vagrant/demo1/target/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar

SuccessExitStatus=143
TimeoutStopSec=10
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target

EOF
  sudo systemctl daemon-reload
  sudo systemctl enable petclinic.service
  sudo systemctl start petclinic
  #sudo systemctl status petclinic

SCRIPT

Vagrant.configure("2") do |config|

  config.vm.provision "shell", privileged: false, inline: $global
  config.vm.synced_folder ".", "/vagrant", type: "nfs"
  config.vm.boot_timeout = 1800
  config.vm.box_download_insecure = true
  if Vagrant.has_plugin?("vagrant-hostmanager")
     config.hostmanager.enabled = true
     config.hostmanager.manage_host = true
     config.hostmanager.ignore_private_ip = false
     config.hostmanager.include_offline = true
  end
  
  config.vm.define :DB_VM do |db| 
       db.vm.box = "ubuntu/focal64"
       db.vm.hostname = "db"
       db.vm.provision :shell, path: "db.sh"
       db.vm.provision :shell, path: "check-mysql.sh"
       db.vm.network :private_network, ip: "192.168.33.10" 
       db.vm.network "forwarded_port", guest: 80, host: 8082,
       auto_correct: true
       db.vm.usable_port_range = 8000..8999
       db.vm.provider "virtualbox" do |vb|
          vb.name = "DB_VM"
          #vb.gui = true
          vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
          vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
          vb.customize ["modifyvm", :id, "--ioapic", "on"]
          vb.memory = "3072"
          vb.cpus = "3"
       end
  end 

  config.vm.define :APP_VM do |app| 
        app.vm.box = "ubuntu/focal64" 
        app.vm.hostname = "app"
        app.vm.provision "shell", inline: $initScript
        app.vm.provision "shell", inline: $systemd
        app.vm.network :private_network, ip: "192.168.33.11" 
        app.vm.network "forwarded_port", guest: 8080, host: 8081,
        auto_correct: true 
        app.vm.usable_port_range = 8000..8999
        app.vm.provider "virtualbox" do |vb|
            vb.name = "APP_VM"
            vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
            vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
            vb.customize ["modifyvm", :id, "--ioapic", "on"]
            vb.memory = "3072"
            vb.cpus = "3"
        end
  end
end
